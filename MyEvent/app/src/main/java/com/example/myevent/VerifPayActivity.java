package com.example.myevent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myevent.Model.ListPay;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;

public class VerifPayActivity extends AppCompatActivity {

    public static final String EXTRA_BANK = "extra_bank";
    public static final String EXTRA_HARGA = "extra_harga";
    public static final String EXTRA_ID_BANK = "extra_id_bank";
    public static final String EXTRA_NAMA = "extra_nama";
    public static final String EXTRA_USER = "extra_user";
    public static final String EXTRA_ID_EVENT = "extra_event";

    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verif_pay);


        mDatabase = FirebaseDatabase.getInstance().getReference();
        //perintah utk cari element textview di xml
        TextView tvHarga = (TextView)findViewById(R.id.tvHarga);
        //mengeluarkan datanya
        tvHarga.setText(getIntent().getStringExtra(EXTRA_HARGA));
        //mencari button bayar di xml nya
        Button btnBayar = (Button)findViewById(R.id.btnBayar);
        Button btnkembali = (Button)findViewById(R.id.btnkembali);

        //memberi rekasi button saat kita menekan buttonnya
        btnBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //constan field untuk menerima data/mengambil data pada activity yg dituju
                //cth:PaymentActivity
                String method = getIntent().getStringExtra(EXTRA_BANK);
                final String mNama = getIntent().getStringExtra(EXTRA_NAMA);
                final String mHarga = getIntent().getStringExtra(EXTRA_HARGA);
                String id=  getIntent().getStringExtra(EXTRA_ID_BANK);
                String id_event=  getIntent().getStringExtra(EXTRA_ID_EVENT);
                final String user = getIntent().getStringExtra(EXTRA_USER);
                String status = "Menunggu Verifikasi";

                ListPay listdata = new ListPay(id,id_event ,mNama, mHarga,user,method,status);
                mDatabase.child("Pesanan").child(id).setValue(listdata).
                        addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Toast.makeText(VerifPayActivity.this, "Success", Toast.LENGTH_SHORT).show();
                                Intent main = new Intent(getApplicationContext(),VerifiedActivity.class);
                                startActivity(main);
                            }
                        });

            }
        });


        btnkembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent main = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(main);
            }
        });

    }
}
