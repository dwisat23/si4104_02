package com.example.myevent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myevent.Model.ListEvent;
import com.example.myevent.Model.ListPay;
import com.example.myevent.Model.getLogin;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class PaymentActivity extends AppCompatActivity {

    public static final String EXTRA_NAMA = "extra_nama";
    public static final String EXTRA_HARGA = "extra_harga";
    public static final String EXTRA_ID = "extra_id";

    TextView tvHarga,tvHarga2;

    private DatabaseReference mDatabase;
    getLogin getlogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        //buat manggil firebasenya
        mDatabase = FirebaseDatabase.getInstance().getReference();

        tvHarga = (TextView)findViewById(R.id.tvHarga);
        tvHarga2 = (TextView)findViewById(R.id.tvHarga2);

        tvHarga.setText(getIntent().getStringExtra(EXTRA_HARGA));
        tvHarga2.setText(getIntent().getStringExtra(EXTRA_HARGA));

        //nyari elemen button dari xml nya
        RelativeLayout BtnBRI = (RelativeLayout)findViewById(R.id.btnBRI);
        RelativeLayout BtnBCA = (RelativeLayout)findViewById(R.id.btnBCA);

        getlogin = new getLogin(this);

        BtnBRI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//ngasih reaksi ke buttonnya
                final String mNama = getIntent().getStringExtra(EXTRA_NAMA);
                final String mHarga = getIntent().getStringExtra(EXTRA_HARGA);
                final String id=  getIntent().getStringExtra(EXTRA_ID);
                final String method =  "BRI";
                final String user = getlogin.getSPNama();
                final String idP = mDatabase.push().getKey();
                String status = "Belum Dibayar";

                // menampilkan list data pembayarannya (memuat list data)
                ListPay listdata = new ListPay(idP, id ,mNama, mHarga,user,method,status);

                //query utk memanggil data didata firebase, child yaitu dungsi utk memanggilsebuat tabel difirebase
                //tabel pemesanan dengan id listdata
                mDatabase.child("Pesanan").child(idP).setValue(listdata).
                        //memberitahukan jika proses pemesanannya sukses
                        addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                //toast memberikan informasi dalam bentuk text
                                Toast.makeText(PaymentActivity.this, "Success", Toast.LENGTH_SHORT).show();
                                //perpindahan halaman
                                Intent main = new Intent(getApplicationContext(),VerifPayActivity.class);
                                //mengirim data ke activity VerifPay atau activity yg dituju
                                main.putExtra(VerifPayActivity.EXTRA_BANK, method);
                                main.putExtra(VerifPayActivity.EXTRA_HARGA, getIntent().getStringExtra(EXTRA_HARGA));
                                main.putExtra(VerifPayActivity.EXTRA_ID_BANK, idP);
                                main.putExtra(VerifPayActivity.EXTRA_ID_EVENT, id);
                                main.putExtra(VerifPayActivity.EXTRA_NAMA, mNama);
                                main.putExtra(VerifPayActivity.EXTRA_USER, user);
                                startActivity(main);
                            }
                        });


            }
        });

        BtnBCA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });



    }
}
