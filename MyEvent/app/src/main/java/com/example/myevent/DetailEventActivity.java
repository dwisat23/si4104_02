package com.example.myevent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class DetailEventActivity extends AppCompatActivity {
    public static final String EXTRA_NAMA = "extra_nama";
    public static final String EXTRA_HARGA = "extra_harga";
    public static final String EXTRA_ID = "extra_id";

    private TextView tvjudul,tvharga;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_event);

        tvjudul = (TextView)findViewById(R.id.tvJudul);
        tvharga = (TextView)findViewById(R.id.tvHarga);


        tvjudul.setText(getIntent().getStringExtra(EXTRA_NAMA));
        tvharga.setText(getIntent().getStringExtra(EXTRA_HARGA));



    }

    public void btnPesan(View view) {
        Intent Payment = new Intent(getApplicationContext(),PaymentActivity.class);
        Payment.putExtra(DetailEventActivity.EXTRA_NAMA, getIntent().getStringExtra(EXTRA_NAMA));
        Payment.putExtra(DetailEventActivity.EXTRA_HARGA, getIntent().getStringExtra(EXTRA_HARGA));
        Payment.putExtra(DetailEventActivity.EXTRA_ID, getIntent().getStringExtra(EXTRA_ID));
        startActivity(Payment);
    }
}
